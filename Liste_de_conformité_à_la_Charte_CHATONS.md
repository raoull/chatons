# Liste de conformité à la Charte CHATONS

Réponse à la liste de conformité CHATONS pour l'associaton [Raoull](https://www.raoull.org/).

Les ajouts de Raoull sont **en gras**.

**⚠️ Attention, document en cours d'édition !**


## Légende

❗ Critères importants pouvant potentiellement être bloquants  
ℹ️ Suggestions

La détermination d'un critère comme étant bloquant ou non est à l'appréciation de chaque membre.


## Vérifications préalables

- [x] ❗ Le site indiqué est en ligne à partir du moment où la candidature est postée.\
**=> Accessible sur https://www.raoull.org/**

- [x] ❗ Dans le cadre du traitement de la candidature, au moins un service est testable et accessible par les membres du collectif. Un accès à un compte "démo" satisfait cette contrainte si les services ne sont pas déjà en accès libre.\
**=> Informations pour Mumble : https://www.raoull.org/Mumble (accessible depuis la page d'accueil)**

- [x] ❗ La structure indique qu'elle respecte la charte et ne laisse pas penser qu'elle est déjà membre du collectif (elle peut en revanche indiquer qu'elle est candidate).\
**=> un paragraphe à ce sujet est déjà affiché dans notre présentation sur raoull.org. Un article dédié est en cours de rédaction.**

ℹ️ Si le portail des services est un sous-domaine (ex.: `services.monchaton.com`) :   
        - Vérifier le domaine parent (`monchaton.com`) ainsi que `www.monchaton.com`.  
        - Si les domaines sont utilisés, s'assurer qu'ils ne sont pas en contradiction manifeste avec la Charte.  
            - Vérifier la présence de pisteurs et la conformité du site à la législation de son pays (mentions légales).  
        - Si les domaines sont inutilisés, suggérer une redirection (enregistrement DNS `CNAME`) vers le sous-domaine des services.  

## Publicité / pisteurs / appels tiers

- [x] ❗ Aucun pisteur tiers n'est présent.  
*Ce point est vérifier avec uBlock Origin, Privacy Badger et/ou NoScript en désactivant les protections de Firefox ainsi que d'éventuels PiHole. Les polices et images tierces sont considérées comme des pisteurs potentiels.*\
**=> Nous n'effectuons aucun pistage, et ne recourons pas à la publicité.**
- [x] ❗ Si des statistiques sont faites, elles ne nécessitent pas de consentements préalables des utilisatrices et utilisateurs (https://www.cnil.fr/fr/cookies-traceurs-que-dit-la-loi). \
**=> Nous n'effectuons aucune étude statistique actuellement. Si nous mettions ceci en place à l'avenir, nous n'étudierions aucune donnée personnelle, que des données  générales et aggrégées.**
- [x] ℹ️ Si le CHATON est contraint de communiquer via un réseau social centralisé ou privé, l'information est accessible par un autre biais.  
*Ressource utile : https://antipub.org/resistance-a-l-agression-publicitaire-s-infiltre-dans-les-reseaux-sociaux/* .\
**=> Actuellement, plus aucune activité sur les réseaux privateurs. Le réseau social sur le quel nous sommes le plus actif est le fediverse : https://framapiaf.org/@Raoull**

## Sécurité

- [ ] ❗ Les utilisateurs et utilisatrices ont accès aux informations principales concernant la politique de sécurité.  
*Si vous traitez des données personnelles ce point est obligatoire d'après la RGPD*\
**=> Article sur la politique de sécurité en cours de rédaction**

### Test des sites web de la plateforme avec [Mozilla Observatory](https://observatory.mozilla.org/)

- [x] ❗ Les sites web du CHATONS et ses services **sont disponibles via HTTPS**.
- [x] ℹ️ Les sites web via HTTP redirigent bien vers HTTPS.
- [x] ℹ️ Les sites web sont notés B ou plus.  
*Si ce n'est pas le cas, suggérer d'appliquer les recommandations de sécurité pour les services web https://ssl-config.mozilla.org/* .\
**=> Note A+ au 31/10/2023, voir : https://observatory.mozilla.org/analyze/raoull.org**

### Test des serveurs SMTP de la plateforme avec [CryptCheck](https://tls.imirhil.fr/) (vérifiable avec `dig MX monchaton.com`) 
- [ ] ℹ️ Les serveurs mails sont notés D ou plus.  
*Si ce n'est pas le cas, suggérer d'appliquer les recommandations de sécurité pour le SMTP*.\
**=> Nous n'avons pas de serveur de courriel actuellement**

### Engagement des adminsys
- [ ] ℹ️ Les personnes qui ont des accès administrateurs, bénévoles ou salariées s'engagent sur une charte concernant des points de confidentialité et de prudence.  
*TODO : comment une personne s'intégre dans le CHATON pour faire des opérations d'adminsys ? Quel compromis sécurité/inclusivité ?*\
**=> Les membres sont validés par l'AG, ils ont d'abord accès aux serveurs de test, et leurs clés SSH ne sont ajoutées aux serveurs de production qu'après une "période d'essai". Nous avons prévu la signature d'une charte mais elle n'est pas encore rédigée.**

### Audit de sécurité avancé (à réaliser avec l'accord du futur CHATONS ?)
- [ ] ℹ️ Avancé : Les autres domaines de la plateforme ne semblent pas contenir de portes dérobées (instances adminer ouvertes), etc.  
*Le certificat TLS utilisé par le site web de la structure candidate peut éventuellement aider à révéler ces sous-domaines. La liste de publication de Let's Encrypt également*.
- [ ] ℹ️ Avancé : Il n'y a pas de port critique laissé ouvert par inadvertance.  
*Pour le vérifier on peut utiliser `nmap`*.
- [ ] ℹ️ Avancé : Le port SSH a été changé.
- [ ] ℹ️ Avancé : Un mécanisme de ban est en place.  
*Si on fait 10 tentatives de mot de passe erronés quelques part, est-ce qu'on peut continuer ?*

**=> À votre disposition pour effectuer l'audit !**

Il convient de discuter directement avec les membres de la structure candidate des précautions à prendre (privilégier l'authentification par clefs publiques, forger une bonne passphrase, chiffrement des disques, politique de gestion des accès, etc).


## Sauvegardes

- [x] ❗ Les informations relatives aux sauvegardes sont indiquées sur le site web de la structure candidate (et pas seulement dans la candidature).\
**=> Page dédiée sur notre wiki : https://wiki.raoull.org/resume-technique/sauvegardes**
- [x] ℹ️ La fréquence des sauvegardes, le nombre de copies, l'étendue des sauvegardes, les techniques et les logiciels utilisés sont fiables.\
**=> Pas de données d'usagers à sauvegarder sur notre service actuel (Mumble). Proxmox Backup Server fiable selon nos tests.**
- [x] ❗ Si la politique de sauvegarde présente un ou des risques inhabituels, les utilisateurs et utilisatrices sont correctement informées des limites et des risques.\
**=> Nos CGU indiquent que nous ne pouvons garantir à 100% les sauvegardes.** 

## Fournisseurs et localisation des données

- [x] ❗ Le site indique les lieux d'hébergement des données (y compris des sauvegardes).\
**=> Sur notre wiki https://wiki.raoull.org/resume-technique/accueil#maitrise-et-localisation-de-l-infrastructure , TODO préciser que notre cible est la sauvegarde multisite**
- [x] ❗ Le site indique le ou les fournisseurs / sous-traitants du futur CHATONS.\
**=> [Le wiki](https://wiki.raoull.org/resume-technique/accueil#maitrise-et-localisation-de-l-infrastructure) indique notre seul sous-traitant : Gandi pour l'hébergement de nos zones DNS**
- [x] ❗ Le site indique le degré de contrôle que le CHATON a sur son infrastructure.  
*Rappel : le CHATON s’engage à afficher publiquement et sans ambiguïté son niveau de contrôle sur le matériel et les logiciels hébergeant les services et les données associées.*\
**=> sur notre wiki https://wiki.raoull.org/resume-technique/accueil#maitrise-et-localisation-de-l-infrastructure**
- [x] ❗ Le ou les fournisseurs / sous-traitants du futur CHATONS ne sont pas incompatibles avec la charte : 
    - Vérifier que la juridiction appliquée à l'hébergement de _chaque serveur_ de la structure lui permet de respecter la Charte.
    - S'assurer, dans la mesure du possible, que la structure n'est pas soumise au Cloud Act et aux autres lois de surveillance américaines.
    - ℹ️ Utiliser `whois` sur l'IP et/ou le domaine du site web pour vérifier la localisation de l'infrastructure.
    - Les serveurs de la structure sont-ils soumis à la juridiction américaine, ou une autre juridiction que celle de la structure ?
    - Les serveurs appartiennent-ils à une société étrangère ? S'ils appartiennent à une société américaine, ils sont soumis au Cloud Act.
    - Peut-on localiser chaque serveur utilisé par la structure ?
- [x] ❗ S'ils le sont, l'information est clairement indiquées sur le site du futur CHATON.  
*Notamment fournisseur de services de paiement en ligne*.\
**=> Seule notre zone DNS est sous-traitée, chez Gandi, société européenne.**
- [x] ❗ Si le CHATON dépend d’un tiers pour héberger ses services (location de serveurs…), la structure est transparente sur les dispositions contractuelles concernant la protection des données hébergées.\
**=> Seul notre wiki interne est temporairement hébergé à l'externe (OVH), son retour sur l'infra Raoull est planifié en novembre 2023. Aucune donnée personnelle d'usager sur le wiki.**
- [x] ℹ️ En cas de location de serveurs (virtuels ou dédiés), le fournisseur s’engage contractuellement à ne pas accéder aux données.\
**=> Pas de location de serveur pour nos services**


## Documentation

Ces informations sont sur le site du futur CHATON, éventuellement dans une rubrique "Documentation".
- [x] ❗ La documentation détaille l'infrastructure globale.
    - Niveau de contrôle sur l'infrastructure (accès root ?).
    - Topologie de l'infrastructure.
    - Forme de l'infrastructure (VPS, hébergement web, serveur dédié ?).
    - Caractéristiques matérielles (processeur, RAM).
    - Système d'exploitation installé sur son ou ses systèmes et sa version, technologies de virtualisation utilisées le cas échéant…
    - ℹ️ Liste des paquets installés.\
**=> Disponible sur https://wiki.raoull.org/resume-technique/ et https://wiki.raoull.org/resume-technique/topologie**
- [x] ❗ La documentation renseigne les procédés nécessaires au déploiement de chaque service.\
**=> Synthétisé dans la partie publique, nos procédures détaillées sont dans l'espace interne du wiki.**
- [x] ℹ️ Les procédures récurrentes du CHATON sont documentées (éventuellement dans une partie privée).  
*Exemple : comment faire des sauvegardes, des statistiques, comment créer un compte, un vps à une nouvelle personne…*\
**=> Oui, dans la section interne de notre wiki**
- [ ] ℹ️ Un document ou un paragraphe décrivant les pratiques et mesures de sécurité pour protéger les données et l'infrastructure du futur CHATON existe.\
**=> Article sur la politique de sécurité en cours de rédaction**
- [ ] ℹ️ Un document sur les durées de rétention des logs existe.\
**=> À faire**


## Logiciel libre

- [x] ❗ Les services fonctionnent à l'aide de logiciels libres uniquement.  
*Au sens de la Free Software Foundation, qu’ils soient compatibles ou non avec la licence GPL. À titre exceptionnel, le CHATON peut proposer un service basé sur un logiciel ou une plateforme propriétaire si aucune solution libre ne permet un fonctionnement acceptable du service.* \
**=> Couche applicative et système 100% libre.**
- [x] ❗ Si le CHATON est contraint de dépendre de logiciels ou de plateformes propriétaires, il les liste sur son site web et justifie leur utilisation.\ 
**=> Seuls nos routeurs réseau ne sont pas 100% libres https://wiki.raoull.org/resume-technique/accueil#technologies-routeur**
- [x] ❗ Sur le site, il y a un lien vers le code source de chaque service.\
**=> Oui, par exemple : https://www.raoull.org/Le-mumble-documentation-technique**
- [x] ❗🔍 le CHATON s’engage à proposer des formats ouverts dans la communication avec ses bénéficiaires.
- [x] ❗ le CHATON propose un moyen de suivre ses actualités en dehors de plateformes propriétaires (via son site web, par exemple).\
**=> Principaux modes de communication : e-mail, actu sur notre site, fediverse**
- [x] ❗ le CHATON propose un moyen de contact sans avoir à créer un compte sur une plateforme tierce.\
**=> Par e-mail, nous pouvons envisager un formulaire de contact à l'avenir**
- [x] ℹ️ le CHATON travaille à l’émergence de solutions libres pour les logiciels propriétaires qu’il utilise.\
**=> Nous étudions les alternatives libres pour remplacer RouterOS sur nos routeurs Mikrotik. Nous avons expérimenté pfSense.**. 



## CGU & Juridique  
**Attention : les CGU et les mentions légales sont deux textes distincts.**

Vous pouvez trouver un exemple de CGU sur ce lien: https://framagit.org/chatons/CHATONS/-/blob/master/docs/CGU_types.md

### CGU, CGV, CGS ou réglement intérieur
- [x] ❗ Les **Conditions Générales d'Utilisation / CGU** sont accessibles depuis le site web du futur CHATON.\
**=> https://www.raoull.org/Conditions-generales-d-utilisation**
- [x] ❗ Les CGU sont claires et compréhensibles, à la portée de n'importe qui (pas seulement de juristes).\
**=> Nous espérons ! Elles sont inspirées de celles de Framasoft et Chapril**
- [x] ℹ️ Une version courte / condensée est proposée afin de faciliter la compréhension du texte.\
**=> Oui, en début de la page**
- [x] ❗ Les CGU n'excluent pas a priori de potentiels utilisateurs et utilisatrices aux services proposés. Le CHATON peut toutefois définir des « publics cibles » auquel il souhaite s’adresser (par exemple sur des critères de proximité géographique ou autres d’intérêt). Cependant il doit, autant que possible, répondre aux demandes non pertinentes, par exemple en les orientant vers un autre CHATON ou en facilitant l’émergence de structures qui répondraient aux besoins non-satisfaits.\
**=> Aucune exclusion**
- [x] ❗ Il y a une clause « Données personnelles et respect de la vie privée » indiquant clairement quelle est la politique du CHATON concernant les pratiques visées.\
**=> Paragraphe "Données à caractère personnel" avec notre engagement "_Ces données ne sont ni vendues, ni transmises à des tiers, et ne le seront jamais._"**
- [x] ❗ Les tarifs de l'ensemble des offres sont décrits publiquement (éventuellement avec un lien ou en renvoyant vers le site).\
**=> Pas de services payants à aujourd'hui.**
- [x] ❗ Le futur CHATON s’engage dans ses CGU à ne s’arroger aucun droit de propriété des contenus, données et métadonnées produits par les hébergées ou les utilisateurs et utilisatrices.\
**=> paragraphe "Droit d’auteur sur les contenus" contient notre engagement "_Nous ne revendiquons aucun droit sur vos données : textes, images, sons, vidéos, ou tout autre élément, que vous téléchargez ou transmettez depuis votre compte._"**
- [x] ❗ Les CGU permettent aux utilisateurs et utilisatrices d'obtenir la suppression définitive de toute information (comptes et données personnelles) concernant l’hébergé·e, dans la limite des obligations légales et techniques.\
**=> paragraphe "Suppression de compte et de données" contient notre engagement "_Une personne peut demander à tout moment la suppression de son compte et des données associées et l’équipe Raoull s’engage à répondre à la demande en tenant compte des limites possibles du service en question._"**
- [x] ❗ Les CGU prévoient la possibilité pour les hébergé·es de quitter les services en récupérant les données associées dans des formats ouverts, dans la mesure du possible.\
**=> dans le paragraphe "Résiliation", mais mériterait d'être mieux formulé**

### Mentions légales
*Conformément à la [loi](https://www.legifrance.gouv.fr/affichTexteArticle.do?idArticle=JORFARTI000002457442&cidTexte=LEGITEXT000005789847&dateTexte=20190207)* : 

- [x] ℹ️ Les Mentions légales sont accessibles depuis le site web du futur CHATON.\
**=> https://www.raoull.org/Mentions-legales**
- [ ] ℹ️ Les Mentions légales contiennent :
    - Le nom du directeur de publication (dans le cas d'une organisation, la dénomination sociale de la personne morale).
    - Le nom, prénom, adresse et numéro de téléphone de l'hébergeur.
    - Si besoin, des renseignements concernant les droits de propriété intellectuelle (licence des publications) et la politique de stockage des cookies et données personnelles.\
**=> nous sommes notre propre hébergeur mais n'avons pas de numéro de téléphone**

## Contacts humains

- [x] ❗ Le site permet à toutes les personnes hébergées de communiquer avec le futur CHATON en mettant en avant au moins un moyen de le faire.  
*Page de contact, numéro de téléphone, mail, liste de diffusion, forum, ticket support, etc. Attention : concernant certains moyens le public doit pouvoir facilement prendre contact.*\
**=> https://www.raoull.org/contact**
- [x] ❗ Il est possible de rencontrer physiquement, d'échanger, et de participer au futur CHATON.\
**=> Pour premier contact : permanence mensuelle au Café Citoyen à Lille, plus présence à d'autres évènements. Pour participer : assemblée régulière le 15 de chaque mois, nous contacter pour le détail du RDV**
- [x] ❗ Les échanges avec les autres CHATONS sont bienveillants.\
**=> Aucune raison de sortir les griffes, non ?**
- [x] ❗ Il n'y a pas de pratique manifestement malveillante envers les utilisateurs et utilisatrices dans les CGU.\
**=> Non, conformément à nos objectifs d'émancipation de nos usagers**
- [x] ℹ️ Il n'existe pas de témoignage décrivant un comportement malveillant de la part du futur CHATON.\
**=> Pas à notre connaissance !**


## Accessibilité
\
**=> Pour être honnêtes, nous sommes sensibles à ces problématiques, mais nous n'avons pas encore eu le temps de faire un audit poussé.**

Une bonne source de renseignement: https://developer.mozilla.org/fr/docs/Accessibilit%C3%A9

### A11y & site du CHATONS
- [x] ℹ️ Le rôle des éléments correspond bien à ce qu'ils représentent visuellement.   
*Par exemple une balise ```<a>``` transformée en bouton doit contenir l'attribut ```role="button"```. Il faut utiliser les balises titres ```<hX>``` plutôt qu'' agrandir la police d'une ```<span>```*.
- [x] ℹ️ Les couleurs du site sont suffisamment contrastées afin de ne pas discriminer les personnes malvoyantes.
*La [mesure du contraste](https://developer.mozilla.org/en-US/docs/Web/Accessibility/Understanding_WCAG/Perceivable/Color_contrast) intégrée aux outils de développement de Firefox peut être utile.
L'outil libre [ColorOracle](https://colororacle.org/) peut aussi être utile.*\
**=> Au moins un membre de l'équipe est daltonien, pas de difficulté constatée pour le moment !**
- [x] ℹ️ Les images nécessaires pour comprendre la page comportent un texte alternatif avec l'attribut `alt`, les images uniquement décoratives contiennent `alt=""`.
- [x] ℹ️ La page accepte que les utilisateurs et utilisatrices puissent remplacer les styles.  
*Une feuille de style est utilisée plutôt que des attributs `style=""` permetant de surcharger le style (daltoniens, mal-voyant)*.
- [ ] ℹ️ Suggestion : Lors de l'accès aux services une information sur l'accessibilité du service est donnée.  
*Il pourrait s'agir d'une icône avec une infobulle*.

### A11y & services du CHATONS
- [ ] ℹ️ Le CHATON fait des Merge Request d'accessibilité sur les services mis en place.

### Accessibilité mobile
- [x] ℹ️ Le site est un minimum *responsive* (lisible sur téléphone) sans problème de navigation.\
**=> Nous n'avons pas constaté de difficulté à l'usage**

### Accessibilité ADSL < 2Mbps
- [x] ℹ️ Les éléments du site web sont suffisament légers.  
*Poussé à l'extrême : https://solar.lowtechmagazine.com/fr/2018/09/how-to-build-a-lowtech-website.html* .\
**=> Chargement complet (sans cache) : "Terminé en : 2,21 s" en simulant une connexion ADSL dans la console de développement de Firefox**


## Apparence, clarté et finition

- [x] ℹ️ Si la plateforme vise le grand public, l'apparence générale est suffisante pour qu'elle ne soit pas un frein à l'adoption des services.\
**=> Nous essayons !**
- [x] ℹ️ Il n'y a pas de liens morts.\
**=> Pas à notre connaissance !**
- [x] ℹ️ La structure a un logo et une identité visuelle qui lui est propre.\
**=> Notre matou Raoull est un hommage au chanteur [Raoul de Godewarsvelde](https://fr.wikipedia.org/wiki/Raoul_de_Godewarsvelde), originaire de Lille et personnage emblématique de notre région.**
- [x] ❗ Si le service est ouvert sans inscription, le service est accessible via un lien depuis le site web du CHATON.\
**=> Oui, voir page https://www.raoull.org/Mumble accessible depuis la page d'accueil**
- [x] ❗ Si le service est payant ou sur inscription, les modalités d'inscriptions sont claires.\
**=> Non applicable**
- [x] ℹ️ Une page ou un paragraphe décrivant chaque service est présent, avec un éventuel tutoriel afin de guider les utilisateur·ices.\
**=> Oui, voir page https://www.raoull.org/Mumble accessible depuis la page d'accueil**

_Transmettre à la structure candidate des conseils d'UI/UX pouvant améliorer le design des services et/ou du site._\
**=> Volontiers !**


## Annexe : Formulaire de candidature

*Aucun champ n'est obligatoire, remplissez ce que vous pouvez* : 

- Nom du futur CHATON : **Raoull**
- Nom de la structure parente le cas échéant :  **non applicable**  
*Exemple : le CHATON Chapril pour l'association April, ou Bastet pour Parinux.*
- Forme juridique de la structure : **Association loi 1901 déclarée**  
*particulier, association, entreprise…*
- Site du futur CHATON : **https://www.raoull.org**
- Site de la structure parente le cas échéant :  **non applicable**
- Mentions légales : **https://www.raoull.org/Mentions-legales**
- Conditions Générales d'Utilisation : **https://www.raoull.org/cgu**
- Journal des incidents et maintenances : **TODO**
- Documentation : **https://wiki.raoull.org/resume-technique/**
- Détails concernant les sauvegardes : **https://wiki.raoull.org/resume-technique/sauvegardes**  
*Lien vers les éléments qui expliquent comment les sauvegardes sont faites (contenant : fréquence des sauvegardes, nombre de copies, étendue des sauvegardes, technique et logiciels utilisés)*.
- Liste des requêtes administratives : **TODO**  
*Précisions sur les requêtes concernant le droit d'auteur : [Framablog](https://framablog.org/2020/04/19/la-reponse-de-lhebergeur-a-la-bergere/), [Forum](https://forum.chatons.org/t/signalement-de-contenus-par-un-editeur/1028/10)*.
- Rapports d'activité : **TODO**  
*Ces rapports peuvent inclure une description de votre activité d'hébergement, mais également d'éducation populaire ou d'autres démarches de sensibilisation.*
- Liste des microcodes propriétaires (le cas échéant) : **TODO**
- Lien vers les informations concernant l'hébergement du CHATON : **https://wiki.raoull.org/resume-technique/**
- Exemple de contribution au libre :  **participation mensuelle à la permanence libriste sur Lille et aux install party avec les amis de Chtinux, Cliss21, CLX, Deuxfleurs... cf. [Agenda du Libre](https://www.agendadulibre.org/tags/Raoull)**  
*Il peut s'agir de merge request, de traductions, de documentations, d'articles, de dons, de souscriptions financières aux éditeurs, d'organisation d'évènements, install party*
- Exemple de démarche d'éducation populaire :  **en plus des actions précédentes, chaque année stand sur la braderie de Lille pour sensibiliser un plus large public**

Le futur CHATON déclare respecter la charte dans l'entiereté des points requis et déclare notamment :  
*Il s'agit ici d'une liste des points qui ne peuvent pas être facilement vérifiés par le collectif.*
- [x] avoir un niveau de contrôle technique suffisant sur son infrastructure pour garantir la confidentialité et la sécurité des données (accès root ? chiffrement ? tiers de confiance ?) ;\
**=> Accès root, disques chiffés**
- [x] donner la priorité aux libertés fondamentales de ses utilisateurs et utilisatrices, notamment le respect de leur vie privée, dans chacune de ses actions ;\
**=> Inscrit en tant qu'objet de l'asso dans ses statuts !**
- [x] ne faire aucune exploitation commerciale des données ou métadonnées des hébergées ;\
**=> Voir engagements dans les CGU**
- [x] ne pratiquer aucune surveillance des actions des utilisateurs et utilisatrices, autre qu'à des fins administratives, techniques ou d'améliorations internes des services ;\
**=> Voir engagements dans les CGU**
- [x] ne pratiquer aucune censure a priori des contenus des hébergées ;
- [x] ne pas répondre aux requêtes administratives ou d’autorité nécessitant la communication d’informations personnelles avant que ne lui soit présentée une requête légale en bonne et dûe forme ;\
**=> Voir engagements dans les CGU**
- [x] appliquer ses CGU avec bienveillance ;
- [x] mettre en œuvre et promouvoir une forme d'organisation et de gouvernance inclusive, capable de s’adapter aux différences et à valoriser la diversité en veillant à ce que les groupes marginalisés ou exclus soient parties prenantes dans les processus de développement ;\
**=> Nos membres pourront témoigner de la facilité à rejoindre l'association et de l'attention donnée à la transparence et la gouvernance collective via nos assemblées régulières**
- [x] avoir un modèle économique basé sur la solidarité ;\
**=> Seuls les adhésions et dons financent l'association**
- [x] proposer des prix raisonnables et en adéquation avec les coûts de mise en œuvre ;\
**=> Non applicable, pas de services payants**
- [x] que le salaire (en équivalent temps plein, primes et dividendes compris) le plus faible de la structure ne saurait être inférieur au quart du salaire (en équivalent temps plein, primes et dividendes compris) le plus élevé de la structure.\
**=> Non applicable, pas de salariés**

Précisions éventuelles si le CHATON n'est pas sûr de respecter pleinement la charte :  \
**=> Réponses effectuées point par point**
